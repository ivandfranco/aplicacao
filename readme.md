## Produto

### GET /produto
Exibe a lista de produtos no sistema.

**Response 200**
```json
[
    {
        "id": 1,
        "nome": "Poupança",
        "rendimento": 0.005
    },
    {
        "id": 2,
        "nome": "CDB",
        "rendimento": 0.007
    },
    {
        "id": 3,
        "nome": "Fundos",
        "rendimento": 0.008
    }
]
```

## Cliente

### GET /cliente
Exibe os dados do cliente logado.

**Response 200**
```json
{
    "nome": "José da Silva",
    "cpf": "312.412.534-56",
}
```

**Response 403**

### POST /cliente
Cadastra um novo cliente no sistema.

**Request Body**
```json
{
    "nome": "José da Silva",
    "cpf": "312.412.534-56",
    "senha": "minhasenha2019"
}
```

**Response 201**
```json
{
    "nome": "José da Silva",
    "cpf": "312.412.534-56",
}
```

### PUT /cliente
Atualiza os dados de um cliente. 

**Request Body**
```json
{
    "nome": "José da Silva",
    "cpf": "312.412.534-56"
}
```
**Response 200**
```json
{
    "nome": "José da Silva",
    "cpf": "312.412.534-56",
}
```
**Response 403**

## Aplicacao

### GET /aplicacao
```json
[
    {
        "id": 1,
        "produto": {
            "id": 2,
            "nome": "CDB",
            "rendimento": 0.007
        },
        "valor": 10000.00,
        "meses": 24
    },
]
```


